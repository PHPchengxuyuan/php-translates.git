<?php
/*
 * @description: 工厂抽象
 * @Author: 1351218627@qq.com
 * @Date: 2021-05-14 19:09:41
 */

namespace app\translates;

use app\modules\mch\controllers\translates\translate\NiuTranslate;
use app\modules\mch\controllers\translates\translate\BaiduTranslate;
use app\modules\mch\controllers\translates\translate\GoogleTranslate;
use app\modules\mch\controllers\translates\translate\Google2Translate;


interface ITranslatectory
{
    // 调用小牛翻译
    public function NiuTranslate($opt) : NiuTranslate;
    // 调用百度翻译
    public function BaiduTranslate($opt) : BaiduTranslate;
    // 调用谷歌翻译
    public function GoogleTranslate() : GoogleTranslate;
    // 调用谷歌2翻译
    public function Google2Translate() : Google2Translate;
}

