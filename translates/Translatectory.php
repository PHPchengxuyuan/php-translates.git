<?php
/*
 * @description: 具体工厂
 * @Author: 1351218627@qq.com
 * @Date: 2021-05-14 19:09:41
 */

namespace app\translates;

use app\modules\mch\controllers\translates\translate\NiuTranslate;
use app\modules\mch\controllers\translates\translate\BaiduTranslate;
use app\modules\mch\controllers\translates\translate\GoogleTranslate;
use app\modules\mch\controllers\translates\translate\Google2Translate;

class Translatectory implements ITranslatectory
{
    //调用小牛翻译
    public function NiuTranslate($opt): NiuTranslate
    {
        return new NiuTranslate($opt);
    }
    //调用百度翻译
    public function BaiduTranslate($opt): BaiduTranslate
    {
        return new BaiduTranslate($opt);
    }
    //调用谷歌翻译
    public function GoogleTranslate(): GoogleTranslate
    {
        return new GoogleTranslate();
    }
    //调用谷歌翻译
    public function Google2Translate(): Google2Translate
    {
        return new Google2Translate();
    }
}
